import express from "express";
import Ciutada from "../models/Ciutada.js";
import Rebut from "../models/Rebut.js";
import Joi from "@hapi/joi";
import mongoose from "mongoose";
import moment from 'moment';
import fs from "fs";

moment().format();

const app = express();

export const crearRebuts = async (req, res) => {
    var numRebuts=0;
    var importRebuts=0;
    try {
        const ciutadans = await Ciutada.find()
        .sort()
        .lean();
        ciutadans.forEach(async function (ciutada, index) {
            try {
                numRebuts+=1;
                let { _id, id_ciutada, nom, cognom1, cognom2, email, genere, dataNaixement, dataUltimRebut, IBAN } = ciutada;
                var _id_ciutada=_id;
                var importPeriode=10;
                var periodicitat='M';
                var dataRebut=moment(dataUltimRebut).add(1,'day');
                var estat='creat';
                importRebuts+=importPeriode
                var dataCobrament=null;
                const rebut = new Rebut({ _id_ciutada, dataRebut, IBAN, importPeriode, periodicitat, estat, dataCobrament});
                await rebut.save();
                dataUltimRebut=moment(dataRebut).add(1,'month').add(-1,'day').format('YYYY-MM-DD');
                await Ciutada.findByIdAndUpdate(ciutada._id, { dataUltimRebut });
            } catch (error) {
                res.status(400).json({"error": true, "missatge": "Error al crear el rebut de "+ciutada._id, "detallError": error})
            }
        });
        res.json({
            error: false,
            missatge: `S'han creat ${numRebuts} rebuts dels ciutadans amb un import total de ${importRebuts} euros`,
        })
    } catch (error) {
        res.status(400).json({"error": true, "missatge": "Error al crear rebuts!", "detallError": error})
    }
};


export const presentarRebuts = async (req, res) => {
    var numRebuts=0;
    var rebutspresentats=[];

    const rebuts = await Rebut.aggregate([
                    {$lookup: {from: "ciutadas", localField: "_id_ciutada", foreignField: "_id", as: "ciutada"}},
                    {$match: {estat : "creat"}},
                ]);
   
    rebuts.forEach(async function (rebut, index) {
        try {
            numRebuts+=1;
            let { _id, _id_ciutada, ciutada, dataRebut, IBAN, importPeriode, periodicitat, estat } = rebut;
            rebutspresentats.push({
                "rebut": _id,
                "id_ciutada": ciutada[0].id_ciutada,
                "nom": ciutada[0].nom,
                "cognoms": ciutada[0].cognom1+' '+ciutada[0].cognom2,
                "data": moment(dataRebut).format('YYYY-MM-DD'),
                "IBAN": IBAN,
                "import": importPeriode,
                "periodicitat": periodicitat
            })
            estat='cobrat';
            await Rebut.findByIdAndUpdate(_id, {estat, $set: {"dataCobrament": moment().format('YYYY-MM-DD')}} ); 
        } catch (error) {
            res.status(400).json({"error": true, "missatge": "Error al crear el rebut de "+rebut._id_ciutada, "detallError": error})
        }
    });

    var json = JSON.stringify(rebutspresentats)
    const nomarxiu='rebuts_'+moment().format('YYYY_MM_DD_HH_mm_ss')+'.txt';

    fs.writeFile(`./src/arxius/${nomarxiu}`, json , (err) => {
        if (err) {
            console.log(err)
            res.end("Error al crear l'arxiu",err);
        } else {
            const {pathname: root} = new URL('..', import.meta.url);
            console.log('root:',root)
            // 2022-05-23 Jaume: te un comportament diferent en dev i prod
            // let arxiu=root.substring(1) + `arxius/${nomarxiu}`;
            // let arxiu=root.substring(0) + `arxius/${nomarxiu}`;
            let arxiu=root.replace('/C:','C:') + `arxius/${nomarxiu}`;
            // console.log('arxiu:',arxiu)
            res.download(arxiu, nomarxiu, function (err) {
                if (err) {
                    console.log('error:',err)
                    // console.log(err)
                    res.status(400).json({"error": true, "missatge": "Error al baixar la cinta", "detallError": err})
                } else {
                    // console.log('tot ok')
                }
            })
        }
    });

};
