import Ciutada from "../models/Ciutada.js";
import Rebut from "../models/Rebut.js";
import Joi from "@hapi/joi";
import mongoose from "mongoose";
import moment from 'moment';

moment().format();

const schemaCiutada = Joi.object({
    id_ciutada: Joi.string().min(1).max(10).required(),
    nom: Joi.string().min(1).max(50).required(),
    cognom1: Joi.string().min(1).max(50).required(),
    cognom2: Joi.string(),
    email: Joi.string().min(6).max(1024).email(),
    genere: Joi.string().min(1).max(1).required(),
    dataNaixement: Joi.date(),
    dataUltimRebut: Joi.date(),
    IBAN: Joi.string().min(24).max(24)
})

// https://zetcode.com/javascript/hapijoi/
// limit: Joi.number().integer().min(1).max(100).default(10)
// password: Joi.string().regex(/^[\w]{8,30}$/),
// married: Joi.boolean().required()
// timestamp: Joi.date().timestamp(),
// const schema = Joi.object().keys({
//     age: Joi.number().min(18).max(129),
//     price: Joi.number().positive(),
//     experience: Joi.number().greater(5)
// });
// price: Joi.number().positive(),
// experience: Joi.number().greater(5)
// const schema = Joi.object().keys({
//     timestamp: Joi.date().timestamp(),
//     isodate: Joi.date().iso(),
//     registered: Joi.date().greater('2018-01-01')
// });
// let timestamp = 1559761841;
// let isodate = '1970-01-19T01:16:01.841Z';
// let registered = '2019-02-12';
// const schema = Joi.array().min(2).max(10);
// const schema = Joi.func().arity(2);


export const llistarCiutadans = async (req, res) => {
    var filtre=req.params.filtre
    if (filtre=='SenseFiltre') filtre=''
    var er = new RegExp(filtre, 'i')
    try {
        const ciutadans = await Ciutada.find({cognom1: er},'id_ciutada nom cognom1 cognom2 dataNaixement email')
        .sort('cognom1')
        .lean()
        .limit(20);
        res.json({
            error: false,
            ciutadans: ciutadans
        })
    } catch (error) {
        console.log(error)
        res.status(400).json({"error": true, "missatge": "Error al preparar el llistat!", "detallError": error})
    }
};

export const contarCiutadans = async (req, res) => {
    try {
        const numerociutadans = await Ciutada.count()
        .sort()
        .lean();
        res.json({
            error: false,
            numerociutadans: numerociutadans 
        })
    } catch (error) {
        console.log(error)
        res.status(400).json({"error": true, "missatge": "Error al contar ciutadans!", "detallError": error})
    }
};


export const crearCiutada = async (req, res) => {
    const { error } = schemaCiutada.validate(req.body);
    if (error) return res.status(400).json({ error: error.details[0].message })
    const { id_ciutada, nom, cognom1, cognom2, email, genere, dataNaixement, dataUltimRebut, IBAN } = req.body;
    const errors = [];
    if (errors.length > 0) {
        res.status(400).json({errors})
    } else {
        try {
            const ciutada = new Ciutada({ id_ciutada, nom, cognom1, cognom2, email, genere, dataNaixement, dataUltimRebut, IBAN });
            await ciutada.save();
            res.json({
                error: false,
                ciutada: ciutada
            })
        } catch (error) {
            console.error(error);
            res.status(400).json({"error": true, "missatge": "Ciutadà no creat!", "detallError": error})
        }
    }
};

export const consultarCiutada = async (req, res) => {
    // console.log(req.params.id)
    try {
        const ciutada = await Ciutada.findById(req.params.id).lean();
        const rebuts = await Rebut.find({ _id_ciutada: req.params.id })
                                  .lean().sort({dataRebut: 'desc'});
        if (ciutada){
            res.json({
                error: false,
                ciutada: ciutada,
                rebuts: rebuts
            });
        } else {
            res.status(400).json({"error": true, "tipuserror": "Id ciutada no existent!"})
        }
    } catch (error) {
        console.log('error',error)
        res.status(400).json({"error": true, "missatge": "Ciutadà no trobat", "detallError": error})
    }
}

export const actualitzarCiutada = async (req, res) => {
    const { error } = schemaCiutada.validate(req.body);
    if (error) return res.status(400).json({ error: error.details[0].message }) 
    const { id_ciutada, nom, cognom1, cognom2, email, genere, dataNaixement, dataUltimRebut, IBAN } = req.body;
    try {
        await Ciutada.findByIdAndUpdate(req.params.id, { id_ciutada, nom, cognom1, cognom2, email, genere, dataNaixement, dataUltimRebut, IBAN });
        res.json({
            error: false,
            missatge: "Ciutadà actualitzat!"
        });
    } catch (error) {
        console.loxxg(error)
        res.status(400).json({"error": true, "missatge": "No s'ha actualitzat el Ciutadà", "detallError": error})
    }
}

export const eliminarCiutada = async (req, res) => {
    try {
        await Ciutada.findByIdAndDelete(req.params.id);
        res.json({
            error: false,
            missatge: "Ciutadà eliminat!"
        });
    } catch (error) {
        console.loxxg(error)
        res.status(400).json({"error": true, "missatge": "No s'ha trobat el ciutadà", "detallError": error})
    }
}

export const crearCiutadans = async (req, res) => {
    var { numeroCiutadans } = req.body;
    if (numeroCiutadans===undefined) numeroCiutadans=100;
    const nomsHomes = ["Adrià", "Agustí", "Albert", "Alex", "Alexandre", "Alexis", "Alonso", "Andreu", "Àngel", "Antoni", "Bautista", "Benici", "Benjamí", "Carles", "Carles Albert", "Carles Eduard", "Carles Robert", "Cèsar", "Cristòbal", "Dani", "David", "Dídac", "Dylan", "Eduard", "Emilia", "Emmanel", "Enric", "Eric", "Ernest", "Fabià", "Felip", "Fèlix", "Ferran", "Francesc", "Francesc Xavier", "Gabriel", "Gaspar", "Gustavu", "Ian", "Iker", "Isaac", "Jacob", "Xavier", "Jeremy", "Jerònim", "Jesús", "Jesús", "Joaquím", "Jordi", "Jordi  Albert", "Jordi Lluís", "Josep", "Josep Antoni", "Josep Daniel", "Josep David", "Josep Francesc", "Josep Gregori", "Josep Lluís", "Josep Manel", "Josep Pau", "Josué", "Joan", "Joan Àngel", "Joan Carles", "Joan David", "Joan Esteban", "Joan Ignaci", "Joan Josep", "Joan Manel", "Joan Pau", "Joan Sebastià", "Juli", "Juli Cesar", "Justí", "Kevin", "Liam", "Lian", "Llorenç", "Lluc", "Lluís", "Lluís Albert", "Lluís Emili", "Lluís Ferran", "Manel", "Manel Antoni", "Marc Antoni", "Mario", "Martí", "Mateo", "Matíes", "Maximilià", "Maykel", "Miquel", "Miquel Àngel", "Nelson", "Noah", "Oscar", "Pau", "Pere", "Rafael", "Ramòn", "Raúl", "Ricard", "Rigobert", "Robert", "Roland", "Samuel", "Samuel David", "Santi", "Sebastià", "Thiago", "Tomàs", "Valentí", "Vicent", "Víctor", "Víctor Hugo"];
    const nomsDones = ["Maria","Carme","Lluisa","Anna","Georgina","Esther","Núria","Eva","Àngela","Mariona","Aina","Joana","Ada","Josefa","Andrea","Sara","Mònica","Rut","Marta","Joaquima","Bet","Elisabet","Miriam","Eva","Framcesca","Cesca","Concepció","Andrea"];
	const cognoms= ["Garcia", "Gonzalez", "Rodriguez", "Fernandez", "Lopez", "Martinez", "Sanchez", "Perez", "Gomez", "Martí", "Jimenez", "Ruiz", "Hernandez", "Diaz", "Moreno", "Alvarez", "Muñoz", "Romero", "Alonso", "Gutierrez", "Navarro", "Torres", "Dominguez",
	"Vazquez", "Ramos", "Gil", "Ramirez", "Serrano", "Blanco", "Suarez", "Molina", "Morales", "Ortega", "Delgado", "Castro", "Ortiz", "Rubio", "Marin", "Sanz", "Nuñez", "Iglesias", "Medina", "Garrido", "Santos", "Castillo", "Cortes", "Lozano", "Guerrero", "Cano", "Prieto", "Mendez", "Calvo", "Cruz", "Gallego", "Vidal", "Leon", "Herrera", "Marquez", "Peña", "Cabrera", "Flores", "Campos", "Vega", "Diez", "Fuentes", "Carrasco", "Caballero", "Nieto", "Reyes", "Aguilar", "Pascual", "Herrero", "Santana", "Lorenzo", "Hidalgo", "Montero", "Ibañez", "Gimenez", "Ferrer", "Duran", "Vicente", "Benitez", "Mora", "Santiago", "Arias", "Vargas", "Carmona", "Crespo", "Roman", "Pastor", "Soto", "Saez", "Velasco", "Soler", "Moya", "Esteban", "Parra", "Bravo", "Gallardo", "Rojas", "Pardo", "Merino", "Franco", "Espinosa", "Izquierdo", "Lara", "Rivas", "Silva", "Rivera", "Casado", "Arroyo", "Redondo", "Camacho", "Rey", "Vera", "Otero", "Luque", "Galan", "Montes", "Rios", "Sierra", "Segura", "Carrillo", "Marcos", "Marti", "Soriano", "Mendoza",
    "Sellarès","Puig","Esteve","Sala","Puigcercòs","Bosc","Serra","Rovira","Renter","Mateu","Codina","Permanyer","Soler","Cucurellas","Reguant","Reverter","Aymerich","Casanoves","Llagostera","Gibert","Masnou","Valls","Argemi","Arenys","Jubany","Perelló","Oses","Puigmal","Cobos","Mateu","Vidal"];
    try {
        for (var i = 1; i <= numeroCiutadans; i++) {
            let id_ciutada= "CAT"+Math.floor(Math.random() * 10000000);
            let sexe=Math.floor(Math.random() * 2);
            if (sexe==1){
                var rand_nom = Math.floor(Math.random()*nomsHomes.length); 
                var nom=nomsHomes[rand_nom];
                var genere="H";
            } else {
                var rand_nom = Math.floor(Math.random()*nomsDones.length); 
                var nom=nomsDones[rand_nom];
                var genere="D";
            }
            let rand_cognom1 = Math.floor(Math.random()*cognoms.length); 
            let rand_cognom2 = Math.floor(Math.random()*cognoms.length); 
            let cognom1=cognoms[rand_cognom1];
            let cognom2=cognoms[rand_cognom2];
            let email=nom.substring(0,1).toLowerCase()+cognom1.toLowerCase()+"@gmail.com";
            let any=Math.floor(Math.random() * 65)+1940;
            let mes=Math.floor(Math.random() * 12)+1;
            let dia=Math.floor(Math.random() * 29)+1;
            let dataNaixement=any+"-"+mes+"-"+dia;
            let dataUltimRebut=moment().startOf('month').add(-1,'day').format('YYYY-MM-DD');
            let IBAN="ES"+(Math.random(1)*10**10)+(Math.random(1)*10**10);
            IBAN=IBAN.replace('.','').substring(0,24);
            let estat='M'; // membre de ple dret
            const ciutada = new Ciutada({ id_ciutada, nom, cognom1, cognom2, email, genere, dataNaixement, dataUltimRebut, IBAN, estat });
            await ciutada.save();
        }
        // console.log(`S'han creat ${i-1} ciutadans!`);
        res.json({
            error: false,
            missatge: `S'han creat ${i-1} ciutadans!`
        });
    } catch (error) {
        console.error(error);
        res.status(400).json({"error": true, "missatge": "Ciutadà no creat!", "detallError": error})
    }
}

export const eliminarCiutadans = async (req, res) => {
    try {
        await Ciutada.collection.drop();
        await Rebut.collection.drop();          
        res.json({
            error: false,
            missatge: "Tots els ciutadans i rebuts eliminats!"
        });
    } catch (error) {
        console.loxxg(error)
        res.status(400).json({"error": true, "missatge": "Error a l'eliminar tots els ciutadans", "detallError": error})
    }
}

