import jwt from 'jsonwebtoken';

const verifyToken = (req, res, next) => {
    const token = req.header('auth-token')
    if (!token) return res.status(401).json({ error: 'Accès denegat' })
    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET)
        req.user = verified
        next() 
    } catch (error) {
        console.log(error)
        res.status(400).json({error: 'El token no és vàlid'})
    }
}

export default verifyToken;