import mongoose from "mongoose";
import dotenv from 'dotenv'

dotenv.config()
try {
    // Conexió a Base de dades
    const uri = `mongodb+srv://${process.env.USER}:${process.env.PASSWORD}@cluster0.vyvqr.mongodb.net/${process.env.DBNAME}?retryWrites=true&w=majority`;
    const option = { useNewUrlParser: true, useUnifiedTopology: true};
    mongoose.connect(uri, option)
    .then(() => console.log('Base de dades conectada:'))
    .catch(e => console.log('error db:', e))
} catch (error) {
    console.error(error);
}
