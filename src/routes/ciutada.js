import { Router } from "express";
import {
    llistarCiutadans,
    contarCiutadans,
    crearCiutada,
    actualitzarCiutada,
    eliminarCiutada,
    consultarCiutada,
    crearCiutadans,
    eliminarCiutadans
} from "../controllers/ciutadans.controller.js";

const router = Router();

router.get('/ping', (req, res) => {
    res.status(200).send("pong!");
});
router.get("/llistar/:filtre", llistarCiutadans);
router.get("/contar", contarCiutadans);
router.get("/:id", consultarCiutada);
router.post("/crear", crearCiutada);
router.post("/crearciutadans", crearCiutadans);
router.put("/actualitzar/:id", actualitzarCiutada);
router.delete("/eliminar/:id", eliminarCiutada);
router.delete("/eliminarciutadans", eliminarCiutadans);

export default router ; 