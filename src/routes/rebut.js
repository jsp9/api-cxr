import { Router } from "express";
import {
    crearRebuts,
    presentarRebuts
} from "../controllers/rebuts.controller.js";

const router = Router();

router.post("/crearrebuts", crearRebuts);
router.get("/presentarrebuts", presentarRebuts);

export default router ; 