import mongoose from "mongoose";

const rebutSchema = mongoose.Schema({
    _id_ciutada: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        min: 1,
        max: 100
    },
    dataRebut: {
        type: Date,
        required: true,
    },
    IBAN: {
        type: String, 
        required: true,
        min: 24,
        max: 24
    },
    importPeriode: {
        type: Number, 
        required: true,
        min: 1,
        max: 1000
    },
    periodicitat: {
        type: String,
        required: true,
        min: 1,
        max: 1
    }, 
    estat: {
        type: String,
        required: true,
        min: 1,
        max: 20
    }, 
    dataCobrament: {
        type: Date
    },
    dataDevolucio: {
        type: Date
    },
    motiuDevolucio: {
        type: String,
        required: false,
        max: 100
    }, 
    create_ad: {
        type: Date,
        default: Date.now
    }
})

export default mongoose.model('Rebut', rebutSchema);