import mongoose from "mongoose";

const ciutadaSchema = mongoose.Schema({
    id_ciutada: {
        type: String,
        required: true,
        min: 1,
        max: 10
    },
    nom: {
        type: String,
        required: true,
        min: 1,
        max: 50
    },
    cognom1: {
        type: String,
        required: true,
        min: 1,
        max: 50
    },
    cognom2: {
        type: String,
        required: false,
        min: 0,
        max: 50
    },
    email: {
        type: String, 
        required: false,
        min: 6,
        max: 100
    },
    genere: {
        type: String,
        required: true,
        min: 1,
        max: 1
    },
    dataNaixement: {
        type: Date
    },
    dataUltimRebut: {
        type: Date
    },
    IBAN: {
        type: String, 
        min: 24,
        max: 24
    },
    estat: {
        type: String, 
        required: true,
        min: 1,
        max: 1
    },
    create_ad: {
        type: Date,
        default: Date.now
    }
})

export default mongoose.model('Ciutada', ciutadaSchema);