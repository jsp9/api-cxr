import express from 'express'; 
import mongoose from 'mongoose';
import bodyparser from 'body-parser';
import cors from 'cors';

// import dotenv from 'dotenv'
// dotenv.config()

const app = express();

// cors
var corsOptions = {
    origin: '*', // Reemplazar con dominio del servidor que consumira l'API (frontend)
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.use(cors(corsOptions));

// capturar body
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

// import routes
import authRoutes from './routes/auth.js';
import ciutadaRoutes from "./routes/ciutada.js";
import rebutRoutes from "./routes/rebut.js";
import verifyToken from './middleware/validate-token.js';

// route middlewares
app.use('/api/user', authRoutes);
app.use('/api/ciutada', verifyToken , ciutadaRoutes);
app.use('/api/rebut', verifyToken , rebutRoutes);

// Middleware para Vue.js router modo history
// const history = require('connect-history-api-fallback');
// app.use(history());

const {pathname: root} = new URL('.', import.meta.url);
app.use(express.static(root + "public"));

app.use((req, res) => {
    res.status(404).json({"missatge": "No trobat!"})
});
  
export default app;
  