import app from "./app.js";
import "./database.js";

const PORT = process.env.PORT || 3000;
console.log("Environment:", process.env.NODE_ENV);

app.listen(PORT, () => {
    console.log(`servidor funcionant al port: ${PORT}`)
})
